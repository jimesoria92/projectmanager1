﻿using EscuelaNet.Dominio.Proyectos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EscuelaNet.Presentacion.Proyectos.Web.Infraestructura
{
    public sealed class Contexto
    {
        private static Contexto _instancia = new Contexto();

        private Contexto()
        {
            
            this.LineasDeProduccion = new List<LineaDeProduccion>();
            LineasDeProduccion.Add(new LineaDeProduccion(){
                Nombre= "Mobile"
            });
            LineasDeProduccion.Add(new LineaDeProduccion()
            {
                Nombre = "API's"
            });
            LineasDeProduccion.Add(new LineaDeProduccion()
            {
                Nombre = "Gestión Empresarial"
            });
        }
        public List<LineaDeProduccion> LineasDeProduccion { get; set; }
        public static Contexto Instancia
        {
            get
            {
                return _instancia;
            }
        }

    }
}