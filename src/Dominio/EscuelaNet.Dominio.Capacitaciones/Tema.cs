﻿using EscuelaNet.Dominio.SeedWoork;

namespace EscuelaNet.Dominio.Capacitaciones
{
    public class Tema : Entity
    {
        public string Nombre { get; set; }
        private Tema()
        {

        }

        public Tema(string nombre) : this()
        { 
            Nombre= nombre;
        }
    }
}