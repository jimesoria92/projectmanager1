﻿using EscuelaNet.Dominio.SeedWoork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EscuelaNet.Dominio.Programadores
{
    public class Conocimiento : Entity
    {
        public string Descripcion { get; set; }
        public Experiencia Grados { get; set; }

        public Conocimiento(string nombre)
        {
            this.Descripcion = nombre ?? throw new System.ArgumentNullException(nameof(nombre));
        }
    }
}
